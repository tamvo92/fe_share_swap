/* eslint-disable no-unused-expressions */
import React, { Component } from "react";

export default class Modal extends Component {
  render() {
    if (this.props.isOpen) {
      if (this.props.step === 1) {
        return (
          <div className="layout-modal">
            <div className="modal">
              <div className="close-button" onClick={this.props.closeModal}>
                <img src={"/image/close@2x.png"} width={24} />
              </div>
              <div>
                <p className="text title">Here's what you need to do next</p>
                <p className="text subtitle">-</p>
                <p className="text color">Transfer your SHR-ERC20</p>
                <p className="text description padding">to</p>
                <p className="text color">{this.props.ethAddress}</p>
              </div>
              <div className="description-bottom step1">
                <p className="text description">After you've completed the transfer go to step 2 and enter your transaction hash</p>
              </div>
              <div className="button-container">
                <button onClick={this.props.handleSubmit} className="btn submit">Continue</button>
              </div>
            </div>
          </div>
        );
      } else {
        return (
          <div className="layout-modal">
            <div className="modal">
              <div className="close-button" onClick={this.props.closeModal}>
                <img src={"/image/close@2x.png"} width={24} />
              </div>
              <img className="icon" src={"/image/mark.png"} alt="mark" width={50} height={50} />
              <div>
                <p className="text title">Thank you!</p>
              </div>
              <div className="description-bottom step2">
                <p className="text description">You will receive your</p>
                <p className="text description">{'SHR BNB tokens on 29-05-2019'}</p>
              </div>
            </div>
          </div>
        );
      }
    }
    return null;
  }
}
