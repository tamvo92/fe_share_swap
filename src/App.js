import React, {Component} from "react";
import Modal from "./modal";
import './layout.css';
import axios from "axios";
import ReactLoading from "react-loading";
const SERVER_NAME = 'http://18.218.188.88:8888';

export default class app extends Component {
  constructor(props) {
    super(props)
    this.state = {
      step: 1,
      bnbAddress: '',
      ethAddress: '',
      amount: '',
      hash: '',
      isOpen: false,
      isLoading: false,
      isSkip: false
    }
  }
  componentDidMount() {
    let isSubmitted = localStorage.getItem('isSubmitted');
    if (isSubmitted) {
      this.setState({
        isSkip: true
      })
    }
  }
  handleChangeText = (e) => {
    const regex = /^[0-9\b]+$/;
    let value = e.currentTarget.value;
    let name = e.currentTarget.name;
    if (name === 'amount' && value && !regex.test(value)) {
      return false;
    }
    this.setState({
      [name]: value
    })
  };
  handleSubmitStep1 = async () => {
    let { bnbAddress, ethAddress, amount } = this.state;
    if (!bnbAddress) {
      this.setState({
        errorMessage: 'The BNB address must be required'
      });
      return false
    }
    if (!ethAddress) {
      this.setState({
        errorMessage: 'The ETH address must be required'
      });
      return false
    }
    if (!amount) {
      this.setState({
        errorMessage: 'The Amount must be required'
      });
      return false
    }
    this.setState({
      errorMessage: '',
      isOpen: true,
    });

  };
  handleSubmitStep2 = async () => {
    let { hash } = this.state;
    if (!hash) {
      this.setState({
        errorMessage: 'The transaction hash must be required'
      });
      return false
    }
    this.setState({
      isLoading: true
    });
    let result = await axios.post(`${SERVER_NAME}/step2`, {
      'txhash': hash
    });
    if (result.data.status) {
      this.setState({
        errorMessage: '',
        isOpen: true,
        isLoading: false,
      })
    } else {
      this.setState({
        errorMessage: result.data.message,
        isLoading: false
      })
    }
  };
  handleSkip = () => {
    this.setState({
      step: 2
    })
  };
  renderFormContainer = () => {
    let { step, bnbAddress, ethAddress, amount, errorMessage, hash, isSkip } = this.state;
    if (step === 1) {
      return (
        <div className="form-container">
          <div className="item">
            <input
              className="input"
              placeholder="Enter BNB address"
              value={bnbAddress}
              name="bnbAddress"
              onChange={this.handleChangeText}
            />
            <div className="text-container">
              <span className="text">If you don't have one yet, go to </span>
              <span className="text link">https://www.binance.org to create a wallet</span>
            </div>
          </div>
          <div className="item">
            <input
              className="input"
              placeholder="Enter ETH address"
              value={ethAddress}
              name="ethAddress"
              onChange={this.handleChangeText}
            />
            <div className="text-container">
              <span className="text">(must be a wallet address you control, not an exchange address)</span>
            </div>
          </div>
          <div className="item">
            <input
              value={amount}
              className="input"
              placeholder="Enter amount of SHR (ERC20) to be swapped for SHR (BEP2)"
              name="amount"
              onChange={this.handleChangeText}
            />
            <div className="text-container">
              <span className="text">(must be a wallet address you control, not an exchange address)</span>
            </div>
          </div>
          <div className="button-container">
            <button onClick={this.handleSubmitStep1} className="btn submit">Submit</button>
            {
              isSkip ?  <button onClick={this.handleSkip} className="btn skip">Skip</button> : null
            }

          </div>
          {
            errorMessage ? (
              <div className="error">
                {errorMessage}
              </div>
            ) : null
          }
        </div>
      )
    } else {
      return (
        <div className="form-container">
          <div className="text-container step2">
            <span className="text description">Please proceed only if you have completed step-1 and received your transaction hash for the completed deposit.</span>
          </div>
          <input
            value={hash}
            className="input step2"
            placeholder="Enter transaction hash"
            name="hash"
            onChange={this.handleChangeText}
          />
          <div className="button-container step2">
            <button onClick={this.handleSubmitStep2} className="btn submit">Submit</button>
          </div>
          {
            errorMessage ? (
              <div className="error">
                {errorMessage}
              </div>
            ) : null
          }
        </div>
      )
    }
  };
  closeModal = () => {
    this.setState({
      isOpen: false,
    })
  };
  handleSubmitModal = async  () => {
    let { bnbAddress, ethAddress, amount } = this.state
    this.setState({
      isLoading: true
    })
    let result = await axios.post(`${SERVER_NAME}/step1`, {
      bnbaddress: bnbAddress,
      ethaddress: ethAddress,
      amount: amount.toString()
    });
    if (result.data.status) {
      localStorage.setItem('isSubmitted', true);
      this.setState({
        errorMessage: '',
        isOpen: false,
        isLoading: false,
        step: 2
      })
    } else {
      this.setState({
        errorMessage: result.data.message,
        isLoading: false,
        isOpen: false
      })
    }
  };
  render() {
    let { step, isOpen, ethAddress, isLoading } = this.state;
    return (
      <div className="app">
        <Modal closeModal={this.closeModal} isOpen={isOpen} step={step} ethAddress={ethAddress} handleSubmit={this.handleSubmitModal}/>
        {
          isLoading ? (
            <div className="loading-container">
              <ReactLoading delay={0} color="#014d9f" width={65} height={65} type={"spinningBubbles"}
                            className="react-loadding"/>
            </div>
          ) : null
        }
        <div className="logo">
          <img src="./image/logo.svg" alt="sharering" />
        </div>
        <div className="content">
          <div className="step-container">
            <div className={step === 1 ?"pointer active" : "pointer"}><span>Step 1</span></div>
            <div className={step === 2 ?"pointer active" : "pointer"}><span>Step 2</span></div>
          </div>
          {
            this.renderFormContainer()
          }
        </div>
      </div>
    )
  }
}
